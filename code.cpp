//
// code.cpp
// Revl
//
// Created by Oliver Dain on 02/15/2018
// Copyright © 2018 Revl. All rights reserved.
//

#include "code.h"

using std::vector;

// A couple comments:
//   It seems the there are basically two overall performance goals for this algorithm:
//     Avoid traversing illegal permutations
//       I've addressed this by in effect treating the permutation space as a tree which
//       traverse in a depth-first fashion
//     Minimize the amount of memory being resized, copied, and otherwise shuffled around
//       I found the interface spec to be a bit constraining in this regard. Once one knows
//       the number of permutations, one may allocate up front all required memory.
//       This gets us the benefit of avoiding any vector capacity resizes/movements, but is
//       incongruent with a vector of vectors interface. The closest approximation I can
//       get is to allocate the the primary vector to the # permutations, and then reserving
//       k space for each permutation. Unfortunately, I don't really have any control as to
//       whether the reservation helps, as this two stage allocation loses all promises of
//       data locality.
//
//       Outside of having more details on how the permutations are being used, it would be
//       nice to provide a class interface that provides methods for traversing the permutations
//       while maintaining a single allocated memory block for all permutations and their
//       elements.
// 
//       Anyways - merry christmas!


// Counts the number of k permutations that can be built from
// camera1 and camera2 moments within the specified spec.
size_t CountPerms(size_t camera_one_size, size_t camera_two_size, int k) {
  size_t count = camera_one_size;

  if (camera_one_size == 0) {
    return 0;
  }

  // Apply the effect of consuming one moment from camera one
  camera_one_size--;
  k--;
  
  // If we have no more camera two elements to draw from, but we still need more elements to complete
  // our permutation, then k is too large, so collapse the count
  if ((camera_two_size == 0) && (k > 0)) {
    return 0;
  }
  
  // If we've completed our permutations
  if (k == 0) {
    return count;
  }

  // We still need to consume more elements to complete the permutation
  return count * CountPerms(camera_two_size, camera_one_size, k);
}

// Generates k-permutations from camera one and camera two moments per the spec. Using iterators
// here to allow for an inline traversal of the preallocated permutations vector
void GeneratePerms(
    const vector<Moment>::const_iterator& camera_one_moments,
    const vector<Moment>::const_iterator& camera_one_end,
    const vector<Moment>::const_iterator& camera_two_moments,
    const vector<Moment>::const_iterator& camera_two_end,
    const int k,
    const VectorOfPermutation::iterator& permutations,
    long long permutation_count) {
  size_t camera_one_size = camera_one_end - camera_one_moments;

  if (camera_one_size == 0) {
    return;
  }

  // We are going to apply the same moment to each of the |stride| permutations
  size_t stride = permutation_count / camera_one_size;
  
  size_t i = 0;
  for (auto moment = camera_one_moments; moment != camera_one_end; moment++) {
    // Append this moment to each of the permutations that contain it    
    for (size_t j = 0; j < stride; j++) {
      (permutations+i*stride+j)->push_back(*moment);
    }

    // If we have more moments to add before these permutations are complete
    if (k > 1) {
      // YUCK - I'm copying the camera one moments in order to drop moment without invalidating the iterators
      // I'm sure there is a more graceful way of doing this, but my kids getting impatient :) 
      vector<Moment> camera_one_less_moment;
      for (auto moment_copy = camera_one_moments; moment_copy != camera_one_end; moment_copy++) {
        if (moment_copy != moment) {
          camera_one_less_moment.push_back(*moment_copy);
        }
      }

      // Complete the permutations covered by this stride
      GeneratePerms(
        camera_two_moments, camera_two_end, camera_one_less_moment.begin(), camera_one_less_moment.end(), 
        k - 1, permutations+i*stride, stride
      ); 
    }

    // Move on to the next chunk of permutations
    i++;
  }
}
   

// Implement your solution here. Feel free to add helper methods, classes, etc.
PtrToVectorOfPermutation AllPossibleEdits(
    const vector<Moment>& camera_one_moments,
    const vector<Moment>& camera_two_moments,
    int k) {
  // Determine how many permutations are possible
  auto n_m_count = CountPerms(camera_one_moments.size(), camera_two_moments.size(), k);
  auto m_n_count = CountPerms(camera_two_moments.size(), camera_one_moments.size(), k);
  size_t count = n_m_count + m_n_count;

  // Allocate memory for our permutations, now that we know how many there will be
  VectorOfPermutation* permutations = new VectorOfPermutation(count, vector<Moment>());
  for (auto permutation = permutations->begin(); permutation != permutations->end(); permutation++) {
    permutation->reserve(k);
  } 

  // Generate the permutations starting with a camera one moment
  GeneratePerms(
    camera_one_moments.cbegin(), camera_one_moments.cend(), camera_two_moments.cbegin(), camera_two_moments.cend(), 
    k, permutations->begin(), n_m_count
  );

  // Generate the permutations starting with a camera two moment
  GeneratePerms(
    camera_two_moments.cbegin(), camera_two_moments.cend(), camera_one_moments.cbegin(), camera_one_moments.cend(),
    k, permutations->begin() + n_m_count, m_n_count
  );

  return PtrToVectorOfPermutation(permutations);
}
